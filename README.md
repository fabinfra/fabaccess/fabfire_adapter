FabFire Adapter
===

The FabFire adapter translates MQTT messages from the FabReader hardware to API calls to bffhd.

# Installation

## With python3-venv

Easy, native installation without overhead is possible with python3 virtual environment:

```shell
cd /opt/fabinfra/adapters/
git clone https://gitlab.com/fabinfra/fabaccess/fabfire_adapter.git --recursive
cd fabfire_adapter/
python3 -m venv env
. env/bin/activate
pip3 install -r requirements.txt
```

## With the Dockerfile and Podman

You can also install the fabfire adapter using Docker or Podman:

```shell
podman build -f Dockerfile -t fabinfra/fabfire_adapter

#cleanup if required:
podman system prune
```

# Configuration

Edit the config file example provided in `config/config.toml`

1. Set `hostname` and `port` for your MQTT Broker and bffhd instance

2. Add your desired readers to the `[readers]` section. Every reader needs a unique subsection name:
   
   ```toml
   [readers]
    [readers.REPLACEME]
    id = "111"
    machine = "urn:fabaccess:resource:Testmachine"
   
    [readers.REPLACEMETOO]
    id = "222"
    machine = "urn:fabaccess:resource:Another"
   ```

# Running

1. Run the adapter:

2. ```shell
   python3 main.py
   ```

  or with Podman:

   ```shell
  podman run localhost/fabinfra/fabfire_adapter:latest
   ```

3. The adapter has to remain running for the readers to work

# Provisioning

See the [provisioning tool](https://gitlab.com/fabinfra/fabaccess/FabFire-Provisioning-Tool) repo.
