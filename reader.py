import asyncio
import logging

import asyncio_mqtt

import fabapi
from host import Host
from timer import Timer


class Reader:
    def __init__(self, reader_id: str, machine_urn: str, bffhd_address: Host):
        self.reader_id = reader_id
        self.machine_urn = machine_urn
        self.bffhd_address = bffhd_address
        self.auth_cap = None
        self.session = None
        self.timeout_timer = None

    async def handle_messages(self, messages, client: asyncio_mqtt.Client):
        try:
            async for message in messages:
                response_for_reader = None
                if not self.auth_cap:
                    self.timeout_timer = Timer(10, lambda: self.handle_timeout(client))
                    self.auth_cap, response_for_reader = await fabapi.connect_with_fabfire_initial(
                        self.bffhd_address.hostname, self.bffhd_address.port, message.payload)
                elif not self.session:
                    response_for_reader, self.session = await fabapi.connect_with_fabfire_step(self.auth_cap,
                                                                                                        message.payload)
                    if self.session:
                        self.timeout_timer.cancel()
                        await client.publish(f"/cmnd/reader/{self.reader_id}", payload='{"Cmd":"haltPICC"}', qos=2,
                                             retain=False)

                        info = self.session.machineSystem.info
                        ma = await info.getMachineURN(f"{self.machine_urn}").a_wait()

                        if ma.which() == "just":
                            ma = ma.just
                        else:
                            logging.critical(f"Could not get machine {self.machine_urn}. Machine does not exist or insufficient permissions")
                            raise Exception(f"Could not get machine {self.machine_urn}. Machine does not exist or insufficient permissions")

                        if ma.state == "inUse":
                            await ma.inuse.giveBack().a_wait()
                            await client.publish(f"/cmnd/reader/{self.reader_id}", payload='{"Cmd":"message","MssgID":0,"AddnTxt":""}', qos=2,
                                                 retain=False)
                        else:
                            await ma.use.use().a_wait()
                            await client.publish(f"/cmnd/reader/{self.reader_id}", payload=response_for_reader, qos=2,
                                                 retain=False)
                            await asyncio.sleep(2)
                            await client.publish(f"/cmnd/reader/{self.reader_id}",
                                                 payload='{"Cmd":"message","MssgID":3,"AddnTxt":""}', qos=2,
                                                 retain=False)
                        self.session = None
                        self.auth_cap = None
                        response_for_reader = None

                if response_for_reader:
                    await client.publish(f"/cmnd/reader/{self.reader_id}", payload=response_for_reader, qos=2, retain=False)
        except Exception as e:
            logging.error(f"Caught exception {e}")
            await client.publish(f"/cmnd/reader/{self.reader_id}", payload='{"Cmd":"haltPICC"}', qos=2,
                                 retain=False)
            self.session = None
            self.auth_cap = None

    async def handle_timeout(self, client):
        await client.publish(f"/cmnd/reader/{self.reader_id}", payload='{"Cmd":"haltPICC"}', qos=2,
                             retain=False)
        logging.critical(f"authentication timed out on reader {self.reader_id}")
        self.auth_cap = None
        self.session = None