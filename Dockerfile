FROM python:3.12.9-bullseye
WORKDIR /app
COPY requirements.txt requirements.txt
RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install -y cmake capnproto clang gcc build-essential
RUN pip3 install -r requirements.txt
COPY . .
VOLUME /app/config
CMD [ "python3", "main.py"]
