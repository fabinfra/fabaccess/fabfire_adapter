import asyncio
from typing import Callable

import asyncio_mqtt


class Timer:
    def __init__(self, timeout: int, callback: Callable):
        self._timeout = timeout
        self._callback = callback
        self._task = asyncio.ensure_future(self._job())

    async def _job(self):
        await asyncio.sleep(self._timeout)
        await self._callback()

    def cancel(self):
        self._task.cancel()
